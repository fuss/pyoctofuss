#!/usr/bin/env python3

from distutils.core import setup

setup(
    name='pyoctofuss',
    version="1.10.2",
    description='Octofuss common library',
    author=['Enrico Zini','Christopher Gabriel'],
    author_email=['enrico@truelite.it', 'cgabriel@truelite.it'],
    url='https://work.fuss.bz.it/projects/pyoctofuss/',
    packages=['octofuss', 'octofuss.samba'],
)
