#  File: error.py
#
#  Copyright (C) 2006 Iacopo Pecchi <iacopo@truelite.it>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License.
# coding: utf-8
from __future__ import (absolute_import, print_function, division, unicode_literals)
import xmlrpc.client
import socket
from gettext import gettext as _


def getConnection(hostname):
    #Returns the rpc server
    server_hostname = hostname
    socket.setdefaulttimeout(0.1)
    conn_string = "http://%s:13401/desktopcontrol" % hostname
    server = xmlrpc.client.Server(conn_string)
    return server

def machine_status(hostname):
    try:
        conn = getConnection(hostname)
        return conn.machine_status()
    except:
        return None

def screen_is_locked(hostname):
    """ Lock a remote desktop """
    try:
        conn = getConnection(hostname)
        return conn.screen_is_locked()
    except:
        return None


def screen_lock(hostname,message):
    """ Lock a remote desktop """
    try:
        conn = getConnection(hostname)
        conn.screen_lock(message)
    except:
        pass


def screen_release(hostname):
    """ Unlock a remote desktop """
    conn = getConnection(hostname)
    try:
        conn.screen_release()
    except:
        pass


def popup_message(hostname,message):
    """ Popup a message on remote desktop """
    conn = getConnection(hostname)
    try:
        conn.popup_message(message)
    except:
        pass


def logged_user(hostname):
    """ Return the remotely logged user """
    conn = getConnection(hostname)
    try:
        return conn.logged_user()
    except:
        return None
