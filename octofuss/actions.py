# coding: utf-8
from __future__ import (absolute_import, print_function, division, unicode_literals)
import octofuss
import os.path

class Action(octofuss.PyTree):
    """
    Quick way to create leaf nodes that perform actions.

    Action objects are mounted as ``host|cluster/:actions:/action``.

     * get: returns a dict with:
       * type: nudge, choice, freeform
       * label: short description of the action
       * desc: long description of the action
       * path: path to query to list available choices (choice only)
       * text: description of the free form value (for example, label of a text
               field) (freeform only)
    """
    def __init__(self, name, info):
        """
         * name is the node name
         * info is the dict returned when querying action information
         * func is the function that peforms the action
        """
        super(Action, self).__init__(name)
        self.info = info

    def _has(self):
        return True

    def _get(self):
        "Retrieve the action information dictionary"
        self.updateInfo();
        return self.info

    def _set(self, value):
        "Perform the action"
        return self.perform(value)

    def _doc(self):
        # Short description
        if "label" in self.info:
            doc = self.info["label"] + " action"
        else:
            doc = "Action"

        if "desc" in self.info:
            doc += "\n\n" + self.info["desc"]

        type = self.info.get("type", "missing")
        doc += "\n\nAction type: %s. " % type
        if type == "nudge":
            doc += "To perform the action, set the action node with any value."
        elif type == "choice":
            doc += """To perform the action, set the action node with any value
chosen among those found in the 'options' key of the action information."""
        elif type == "freeform":
            doc += "To perform the action, set the action node with the value entered by the user."
        elif type == "completion":
            doc += """To perform the action, set the action node with the value entered by the user.
Possible values for autocompletion can be listed at path %s relative to the action.""" % self.info.get("path", "(missing: this is a bug)")
        else:
            doc += "Unknown action type: %s" % type

        if "text" in self.info:
            doc += "\n\nMeaning of the value: %s" % self.info["text"]

        return doc

    def enabled(self):
        return True

    def updateInfo(self):
        "Update the object information"
        pass

    def perform(self, value):
        pass


class ActionTree(octofuss.Tree):
    def __init__(self, name, doc=None):
        super(ActionTree, self).__init__(name, doc)
        self.actions = dict()
    def register(self, action):
        self.actions[action._name] = action
    def lhas(self, path):
        """
        Return True if the path exists, else False
        """
        if len(path) == 0: return True
        action = self.actions.get(path[0], None)
        if action == None: return False
        if len(path) == 1: return action.enabled()
        return action.lhas(path[1:])
    def lget(self, path):
        if len(path) == 0:
            return dict([(n, a.lget([])) for n, a in iter(list(self.actions.items())) if a.enabled()])
        action = self.actions.get(path[0], None)
        if action == None: return None
        return action.lget(path[1:])
    def lset(self, path, value):
        if len(path) == 0: raise AttributeError("cannot set the value of path %s" % self._name)
        action = self.actions.get(path[0], None)
        if action == None: raise LookupError("path %s does not exist" % os.path.join(self._name, path[0]))
        return action.lset(path[1:], value)
    def llist(self, path):
        if len(path) == 0: return sorted([x for x,y in iter(list(self.actions.items())) if y.enabled()])
        action = self.actions.get(path[0], None)
        if action == None: raise LookupError("path %s does not exist" % os.path.join(self._name, path[0]))
        return action.llist(path[1:])
    def ldoc(self, path):
        if len(path) == 0: return super(ActionTree, self).ldoc(path)
        action = self.actions.get(path[0], None)
        if action == None: return None
        return action.ldoc(path[1:])
