# coding: utf-8
from __future__ import (absolute_import, print_function, division, unicode_literals)
import time
import datetime
import pytz

def dump_datetime(d):
    "Convert a datetime into a string using the UTC timezone"
    if d.tzinfo is not None:
        d = d.astimezone(pytz.utc)
    return d.strftime("%Y-%m-%d %H:%M:%S")

def load_datetime(d):
    "Convert a string in UTC to a datetime with timezone"
    d = datetime.datetime(*(time.strptime(d, "%Y-%m-%d %H:%M:%S")[0:6]))
    return d.replace(tzinfo=pytz.utc)
