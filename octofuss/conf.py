# coding: utf-8
from __future__ import (absolute_import, print_function, division, unicode_literals)
import os
from six.moves.configparser import ConfigParser
import io

def readConfig(rootDir = None, defaults = None, nick="octofuss"):
    """
    Read octofuss configuration, returning a ConfigParser object
    """
    if rootDir == None:
        rootDir = os.environ.get(nick.upper() + "_CONFDIR", "/etc/" + nick)
    files = []
    def trytouse(path):
        if os.path.exists(path):
            files.append(path)

    # Start with the main config file
    trytouse(os.path.join(rootDir, nick + ".conf"))

    # Add snippets found in rc.d style directory
    subdir = os.path.join(rootDir, nick + ".conf.d")
    if os.path.isdir(subdir):
        for file in sorted(os.listdir(subdir)):
            if file.startswith('#'): continue
            if file.startswith('.'): continue
            if file.endswith('~'): continue
            if file.endswith('.bak'): continue
            trytouse(os.path.join(subdir, file))

    config = ConfigParser()
    if defaults != None:
        infile = io.StringIO(defaults)
        config.read_file(infile, "defaults")
    config.read(files)
    return config
