# coding: utf-8
from __future__ import (absolute_import, print_function, division, unicode_literals)
import xmlrpc.client
import sys
import socket

def list_cluster(server_url):
    servers = []
    tmpserver = xmlrpc.client.ServerProxy(server_url,
                                      xmlrpc.client.Transport(),
                                      "UTF-8")
    s = tmpserver.list_clusters()
    if isinstance(s, list):
        for ns in s:
            servers.append(ns)
    servers.sort()
    return servers

def find_broadcast():
    import netifaces
    addresses = []
    for iface in netifaces.interfaces():
        # only get ipv4 (netifaces.AF_INET) addresses, ipv6 ones would
        # be in netifaces.ifaddresses(iface)[netifaces.AF_INET6]
        for addr in netifaces.ifaddresses(iface).get(netifaces.AF_INET, []):
            bcast = addr.get('broadcast')
            if bcast:
                addresses.append(bcast)
    return addresses

def discover_server(timeout=5):
    PORT = 13400
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.bind(('', PORT+1))
    s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
    baddrs = find_broadcast()
    servers = []
    for baddr in baddrs:
        data = b"client-request"
        s.sendto(data, (baddr, PORT))
        # wait 5 seconds for the answer
        s.settimeout(timeout)
        try:
            data, fromaddr = s.recvfrom(1024)
            if fromaddr:
                server = fromaddr[0]
                if server not in servers:
                    servers.append(server)
        except socket.timeout as e:
            pass
    return servers
