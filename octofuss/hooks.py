# coding: utf-8
from __future__ import (absolute_import, print_function, division, unicode_literals)
import os, os.path, sys
from subprocess import Popen, PIPE

conf_hookdir = None

def configure(hookdir = None):
    global conf_hookdir
    conf_hookdir = hookdir

def invoke(script, *args):
    hookscript = os.path.join(conf_hookdir, script)
    if not os.path.exists(hookscript):
        raise RuntimeError("Hook script %s does not exist" % hookscript)
    proc = Popen([hookscript] + list(map(str, args)), stdout=open("/dev/null"), stderr=PIPE)
    stdout, stderr = proc.communicate()
    if proc.returncode != 0:
        raise RuntimeError(str(stderr))
