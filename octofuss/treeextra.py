# coding: utf-8
from __future__ import (absolute_import, print_function, division, unicode_literals)
import octofuss

class ListTree(octofuss.Tree):
    def __init__(self, factory, name = None, doc=None):
        super(ListTree, self).__init__(name, doc)
        self.factory = factory

    def child_data_by_name(self, name):
        "Override to get child element data by name"
        return None

    def list_child_data(self):
        "Override to list child element data"
        return []

    def create_child_data(self, name):
        "Override to create a new child and return its data"
        return None

    def delete_child_data(self, data):
        "Override to delete a child given its data"
        pass

    def lhas(self, path, **kw):
        if len(path) == 0: return True
        data = self.child_data_by_name(path[0])
        if data is None: return False
        if len(path) == 1: return True
        return self.factory(data).lhas(path[1:], **kw)

    def lget(self, path, **kw):
        if len(path) == 0: return [self.factory(data).lget([], short=True) for data in self.list_child_data()]
        data = self.child_data_by_name(path[0])
        if data is None: return None
        return self.factory(data).lget(path[1:], **kw)

    def ldoc(self, path, **kw):
        if len(path) == 0: return super(ListTree, self).ldoc(path, **kw)
        data = self.child_data_by_name(path[0])
        if data is None: return None
        return self.factory(data).ldoc(path[1:], **kw)

    def llist(self, path, **kw):
        if len(path) == 0: return [data.name for data in self.list_child_data()]
        data = self.child_data_by_name(path[0])
        if data is None: return []
        return self.factory(data).llist(path[1:], **kw)

    def lset(self, path, value, **kw):
        if len(path) == 0:
            return super(ListTree, self).lset(path, value, **kw)
        data = self.child_data_by_name(path[0])
        if data is None:
            return super(ListTree, self).lset(path, value, **kw)
        return self.factory(data).lset(path[1:], value, **kw)

    def lcreate(self, path, value=None, **kw):
        if len(path) == 0:
            return super(ListTree, self).lcreate(path, value, **kw)
        if len(path) == 1:
            data = self.create_child_data(path[0])
            return self.factory(data).lget([], **kw)
        data = self.child_data_by_name(path[0])
        return self.factory(data).lcreate(path[1:], value, **kw)

    def ldelete(self, path, **kw):
        if len(path) == 0:
            return super(ListTree, self).ldelete(path, **kw)
        data = self.child_data_by_name(path[0])
        if data is None:
            return super(ListTree, self).ldelete(path, **kw)
        if len(path) == 1:
            self.delete_child_data(data)
        else:
            return self.factory(data).ldelete(path[1:], **kw)

