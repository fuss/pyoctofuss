#  File: plugins.py
# 
#  Copyright (C) 2008 Christopher R. Gabriel <cgabriel@truelite.it> 
# 
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
# 

# coding: utf-8
from __future__ import (absolute_import, print_function, division, unicode_literals)
import os
import os.path
import importlib
import sys

def load_plugins(pluginpath=None, nick="octofuss"):
    """return a list of all available plugins
    in the given directory"""

    if not pluginpath:
        pluginpath = os.environ.get(nick.upper() + "_PLUGINS", "plugins")

    # if the plugin path need to be auto-discovered
    # starting from a specified module
    #pluginpath = os.path.join(os.path.dirname(imp.find_module("octofussd")[1]), "extensions/")
    #pluginpath = "."
    pluginfiles = [fname[:-3] for fname in os.listdir(pluginpath) if fname.endswith(".py") and not fname.startswith(".") and not fname.endswith("~")]

    for fname in pluginfiles:
        oldpath = sys.path
        try:
            sys.path.append(os.path.abspath(pluginpath))
            # https://docs.python.org/3.12/library/importlib.html#importing-a-source-file-directly
            spec = importlib.util.spec_from_file_location(
                fname, os.path.join(pluginpath, fname) + ".py"
            )
            module = importlib.util.module_from_spec(spec)
            sys.modules[fname] = module
            spec.loader.exec_module(module)
            res = module
        finally:
            sys.path = oldpath
        yield res
            
