# coding: utf-8
from __future__ import (absolute_import, print_function, division, unicode_literals)
from json import dumps, loads
from six.moves.xmlrpc_client import ServerProxy
import octofuss
import octofuss.exceptions

class UnknownException(Exception):
    def __init__(self, msg):
        Exception.__init__(self, msg)


class APIKeyException(Exception):
    def __init__(self, msg):
        Exception.__init__(self, msg)


class ExceptionSerializer:
    EXCEPTION_WHITELIST = [
        Exception, UnknownException, LookupError, AttributeError, ValueError, KeyError, APIKeyException,
    ]

    def __init__(self):
        self.by_class = { c: c.__module__ + "." + c.__name__ for c in self.EXCEPTION_WHITELIST }
        self.by_name = { c.__module__ + "." + c.__name__: c for c in self.EXCEPTION_WHITELIST }

        # Add the exceptions from octofuss.exceptions
        for name in octofuss.exceptions.__all__:
            exc = getattr(octofuss.exceptions, name)
            name = "octofuss.exceptions." + name
            self.by_class[exc] = name
            self.by_name[name] = exc

    def exception_name(self, exc):
        name = self.by_class.get(exc)
        if name is not None:
            return name
        print("Exception", exc, "is not whitelisted")
        for b in exc.__bases__:
            ub = self.exception_name(b)
            if ub != None:
                return ub
        return self.by_class[UnknownException]

    def to_dict(self, exc):
        # Find a useable exception name
        name = self.exception_name(exc.__class__)
        message = getattr(exc, "message", None)
        if message is None: message = str(exc)
        return {"name": name, "msg": message}

    def from_dict(self, d):
        name = d.get("name")
        if name is None:
            cls = UnknownException
        else:
            cls = self.by_name.get(name)
            if cls is None:
                cls = UnknownException

        msg = d.get("msg", "No error message provided")
        return cls(msg)


exception_serializer = ExceptionSerializer()


class Client(octofuss.Tree):
    def __init__(self, url, apikey = ""):
        super(Client, self).__init__()
        self.url = url
        self.server = ServerProxy(url)
        self.apikey = apikey

    def _wrap_call(self, callable, *args):
        res = loads(callable(self.apikey, *args))
        if "exc" in res:
            raise exception_serializer.from_dict(res["exc"])
        elif "res" in res:
            return res["res"]
        else:
            raise ValueError("value sent from server has neither an 'ext' nor a 'res' part")

    def has(self, path):
        return self._wrap_call(self.server.has, path)

    def lhas(self, path):
        return self._wrap_call(self.server.has, "/".join(path))

    def get(self, path):
        return self._wrap_call(self.server.get, path)

    def lget(self, path):
        return self._wrap_call(self.server.get, "/".join(path))

    def set(self, path, value):
        return self._wrap_call(self.server.set, path, dumps(value))

    def lset(self, path, value):
        return self._wrap_call(self.server.set, "/".join(path), dumps(value))

    def list(self, path):
        return self._wrap_call(self.server.list, path)

    def llist(self, path):
        return self._wrap_call(self.server.list, "/".join(path))

    def delete(self, path):
        return self._wrap_call(self.server.delete, path)

    def ldelete(self, path):
        return self._wrap_call(self.server.delete, "/".join(path))

    def create(self, path, value=None):
        return self._wrap_call(self.server.create, path, dumps(value))

    def lcreate(self, path, value=None):
        return self._wrap_call(self.server.create, "/".join(path), dumps(value))

    def doc(self, path):
        return self._wrap_call(self.server.doc, path)

    def ldoc(self, path):
        return self._wrap_call(self.server.doc, "/".join(path))


try:
    from twisted.web import xmlrpc

    class Server(xmlrpc.XMLRPC):
        """
        Export an octofuss.Tree over XMLRPC
        """
        def __init__(self, service, tree):
            xmlrpc.XMLRPC.__init__(self)
            self.service = service
            self.tree = tree

        def render(self, request):
            # https://twistedmatrix.com/documents/8.1.0/api/twisted.web.http.Request.html#getClient
            # https://twistedmatrix.com/documents/8.1.0/api/twisted.web.http.Request.html#getClientIP
            # DON'T use undocumented getClient() that (inferred empirically, as it is *undocumented*)
            # return a string telling the IP that the request came from.
            # This code broke every request (even login ones) when using twisted 18.9.x (as of debian buster)
            # instead of twisted 16.6.x (as of stretch).
            # Using getClientIP() apparently fixed things, but I hope that every code that uses this class
            # will be happy with my modification.
            self.client = request.getClientIP()
            return xmlrpc.XMLRPC.render(self, request)

        def _validate_apikey(self, apikey):
            return True

        def _validate_access(self, apikey, path):
            return True

        def _wrap_call(self, apikey, denied_value, callable, *args):
            try:
                if not self._validate_apikey(apikey):
                    raise APIKeyException("Invalid API key")
                if not self._validate_access(apikey, args[0]):
                    return dumps(dict(res=denied_value))
                res = callable(*args)
                # Turn views into lists.
                # Ideally, though, tree methods should return lists, not views
                if type(res) == type({}.keys()):
                    res = list(res)
                return dumps(dict(res=res))
            except Exception as exc:
                import traceback
                traceback.print_exc()
                return dumps({"exc": exception_serializer.to_dict(exc)})

        def xmlrpc_has(self, apikey, path):
            return self._wrap_call(apikey, False, self.tree.has, path)

        def xmlrpc_get(self, apikey, path):
            return self._wrap_call(apikey, None, self.tree.get, path)

        def xmlrpc_set(self, apikey, path, value):
            return self._wrap_call(apikey, False, self.tree.set, path, loads(value))

        def xmlrpc_list(self, apikey, path):
            return self._wrap_call(apikey, [], self.tree.list, path)

        def xmlrpc_delete(self, apikey, path):
            return self._wrap_call(apikey, False, self.tree.delete, path)

        def xmlrpc_create(self, apikey, path, value = None):
            return self._wrap_call(apikey, False, self.tree.create, path, loads(value))

        def xmlrpc_doc(self, apikey, path):
            return self._wrap_call(apikey, False, self.tree.doc, path)

except ImportError:
    # Only define XMLRPCTreeServer if we have twisted
    pass
