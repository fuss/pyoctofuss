# coding: utf-8

__all__ = ["HomeDirectoryExistsError", "HomeDirectoryOutsideHomeError", "UserAlreadyExistsError", "DeleteSystemGroupError"]


class HomeDirectoryExistsError(FileExistsError):
    pass


# TODO: Find a better suited parent exception class
class HomeDirectoryOutsideHomeError(Exception):
    pass


# TODO: Find a better suited parent exception class
class UserAlreadyExistsError(Exception):
    pass

# added for group delete                       
class DeleteSystemGroupError(Exception):
    pass
