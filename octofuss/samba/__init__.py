# -* python -*-
# -*- coding: UTF-8 -*-
#
#  File: config.py
#
#  Copyright (C) 2006 Iacopo Pecchi <iacopo@truelite.it> 
# 
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License version 2, as
#  published by the Free Software Foundation.

# coding: utf-8
from __future__ import (absolute_import, print_function, division, unicode_literals)
import os, string
from .conf import *
from .parameters import parm_table
#from octofuss.error import MissingPermission
#from octofuss.error import MissingConfigurationFile
#from octofuss.error import BadConfigurationFile
#from octofuss import log

class MissingPermission(Exception): pass
class MissingConfigurationFile(Exception): pass
class BadConfigurationFile(Exception): pass
class Logger:
    def debug(*args):
        print(" ".join(args))
log=Logger()


class ShareModel:
    __sambaConfigFile = ""
      
    __serverParams = ["GLOBAL", "PRINTERS", "PRINT$", "HOMES", "NETLOGON", "PROFILES"]
    #__serverParams = parm_table.keys()
    
    def __init__(self,configfile="/etc/samba/smb.conf"):
        self.__sambaConfigFile = configfile
        if not (os.access(self.__sambaConfigFile,os.F_OK)):
            raise MissingConfigurationFile("Missing configuration file!")

        if not (os.access(self.__sambaConfigFile,os.R_OK) and os.access(self.__sambaConfigFile,os.W_OK)):
            raise MissingPermission("Missing permissions on samba configuration file!")

        self.__conf = SambaConf()
        self.__conf.ReadConfig(self.__sambaConfigFile)
        if not self.__conf.valid:
            raise BadConfigurationFile("Bad configuration file")

    def reload(self):
        del(self.__conf)
        self.__conf = SambaConf()
        self.__conf.ReadConfig(self.__sambaConfigFile)

    def addShare(self, shareObj):
        """ Add a share to the samba configuration file """
        self.reload()
        if not isinstance(shareObj, Share):
            raise TypeError("argument must be a Share instance")
        name = shareObj.getName()
        if string.upper(name) in self.__serverParams:
            log.debug("Share name must be different from samba params!")
            raise NameError("Share name must be different from samba params!")
            return
        if self.__conf.isService(name):
            log.debug("Share already exist!")
            raise NameError("Share already exists")
            return
        if shareObj.checkShare():
            self.__conf.AddService(name) 
            for option in shareObj:
                if shareObj[option] != "":
                    self.__conf.SetServiceOption(name, option, shareObj[option])
            self.__conf.Write(self.__sambaConfigFile)
        else:
            log.debug("Share not valid!")
            raise Error("Share not valid")

    def removeShare(self, shareName):
        """ Remove a share from the samba configuration file """
        self.reload()
        if shareName not in self.__serverParams:
            self.__conf.DelService(shareName)
            self.__conf.Write(self.__sambaConfigFile)
        else:
            log.debug("You can't remove samba server params!")

    def getAllServices(self):
        """ Return a list of Share objects available in smb.conf. """
        self.reload()
        return self.__conf.Services()

    def getShares(self):
        """ Return a list of shares present in samba config file """
        self.reload()
        services = self.getAllServices()
        shares = []
        for service in services:
            if service not in self.__serverParams:
                shares.append(service)
        return shares

    def getShare(self, shareName):
        """ Return a dictionary parameter = value """
        self.reload()
        share = Share(shareName)
        shareDict = self.__conf.getService(shareName)
        for key in shareDict:
            share[shareDict[key].getKey()]=shareDict[key].getValue()
        return share

    def modifyShare(self, share, option, value):
        """ Set an option of a share """
        self.reload()
        if share not in self.__serverParams:
            self.__conf.SetServiceOption(share, option, value)
            self.__conf.Write(self.__sambaConfigFile)
        else:
            log.debug("You can't modify samba server params!")

    def printConf(self):
        self.reload()
        self.__conf.Dump(sys.stdout)



class Share(dict):
    __name = ""
    __state = False
    __requiredOptions = ["path", "read only"]
    __nonRequiredOptions = ["valid users","guest ok", "comment"]

    def __init__(self,name=""):
        dict.__init__(self)
        self.__name = name
        for entry in self.__requiredOptions:
            self[entry]=""
        for entry in self.__nonRequiredOptions:
            self[entry]=""

    def setName(self, name):
        self.__name = name

    def getName(self):
        return self.__name

    def setOption(self, key, value):
        self[key] = value

    def getOption(self, key):
        return self[key]

    def getRequiredOptions(self):
        return self.__requiredOptions

    def setState(self, state):
        self.__state = state

    def getState(self):
        return self.__state

    def checkShare(self):
        """ Check for null values """
        if self.getName() == "":
            raise NameError("Share name must be not null!")
        try:
            for opt in self.__requiredOptions:
                if self[opt]=="":
                    return False

            return True
        except KeyError:
            return False


if __name__ == '__main__':
    a = Share("Copy")
    a.setOption("path","1") 
    a.setOption("comment","1") 
    model = ShareModel()
    #model.printConf() 
    model.addShare(a) 
    model.addShare(a) 

    print(model.getShares())
    #print model.getShares()
    model.removeShare("ciao") 
    print(model.getShares())
    model.addShare(a) 
    print("-----------")
    print(model.getShare("C"))
    print("-----------")

    print(model.getAllServices())
    #model.modifyShare("ciao", "path","2")
    print(model.getShares())
