#!/usr/bin/env python
#
#  File: password.py
# 
#  Copyright (C) 2010 Christopher R. Gabriel <cgabriel@truelite.it> 
# 
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
# 

# coding: utf-8
from __future__ import (absolute_import, print_function, division, unicode_literals)
import os.path
import random

WORDS_FILES = '/usr/share/dict/words'

def generate_simple():
    if os.path.isfile(WORDS_FILES):
        # generate a new simple password based on a word
        words = open(WORDS_FILES).readlines()
        nice_words = [str(x).lower().strip() for x in words if len(x) < 9 and len(x) > 6  and "'" not in x]
        while True:
            sample = random.sample(nice_words,1)[0].strip()
            seed = random.randint(10,100)
            try:
                sample.encode('ascii')
                return sample+str(seed)
            except:
                continue
    else:
        return generate_hard()
    
def generate_hard():
    if os.path.isfile('/usr/bin/pwgen'):
        stdout, dump = popen2(('pwgen','-1'))
        password = str(stdout.read().strip())
        return password
    else:
        return 'fuss'+str(random.randint(1000,10000))
            
def generate_password():
    return generate_simple()
