pyoctofuss (1.10.2) unstable; urgency=medium

  * Bugfix for one of the code updates.

 -- Elena Grandi <elena@truelite.it>  Thu, 21 Nov 2024 13:45:46 +0100

pyoctofuss (1.10.1) unstable; urgency=medium

  * Update code that was using features deprecated early in the python3
    lifecycle.

 -- Elena Grandi <elena@truelite.it>  Fri, 08 Nov 2024 10:46:59 +0100

pyoctofuss (1.10.0) unstable; urgency=medium

  [ Simone Piccardi ]
  * Adding exception to block system group removal
  * Packaging has been updated for bookworm

 -- Elena Grandi <elena@truelite.it>  Mon, 16 Sep 2024 14:51:41 +0200

pyoctofuss (1.9.0) unstable; urgency=medium

  * Update packaging for bullseye

 -- Elena Grandi <elena@truelite.it>  Wed, 18 May 2022 10:40:49 +0200

pyoctofuss (1.8.0) unstable; urgency=medium

  * Update packaging for buster

 -- Elena Grandi <elena@truelite.it>  Fri, 09 Aug 2019 12:57:53 +0200

pyoctofuss (1.7.1) unstable; urgency=medium

  * Use python3-netifaces to get broadcast addresses instead of parsing
    ifconfig output. refs: #541

 -- Elena Grandi <elena@truelite.it>  Thu, 28 Jun 2018 10:48:17 +0200

pyoctofuss (1.7.0) unstable; urgency=medium

  * Package update for Debian 9

 -- Paolo Dongilli <dongilli@fuss.bz.it>  Wed, 17 Jan 2018 18:03:54 +0100

pyoctofuss (1.6.6-1) unstable; urgency=medium

  * fix #192 and code cleanup

 -- Christopher R. Gabriel <cgabriel@truelite.it>  Fri, 28 Apr 2017 10:28:32 +0200

pyoctofuss (1.6.5-1) unstable; urgency=medium

  * fix #196 - python3 porting

 -- Christopher R. Gabriel <cgabriel@truelite.it>  Wed, 22 Feb 2017 12:09:14 +0100

pyoctofuss (1.6.4-1) unstable; urgency=low

  * fix #195 - missing python3-six dep

 -- Christopher R. Gabriel <cgabriel@truelite.it>  Wed, 22 Feb 2017 12:00:17 +0100

pyoctofuss (1.6.3-1) unstable; urgency=low

  * New feedback release

 -- Christopher R. Gabriel <cgabriel@truelite.it>  Tue, 30 Dec 2016 11:31:17 +0100

pyoctofuss (1.6-1) unstable; urgency=low

  * New fixes upon octofussd porting. Moved ldaputils module to octofussd

 -- Christopher R. Gabriel <cgabriel@truelite.it>  Tue, 20 Dec 2016 12:20:38 +0100

pyoctofuss (1.4-1) unstable; urgency=low

  * Repackaged using the new python packaging toolchain

 -- Enrico Zini <enrico@debian.org>  Tue, 06 Dec 2016 13:18:38 +0100

pyoctofuss (1.3) lenny; urgency=low

  * New upstream release

 -- Christopher R. Gabriel <cgabriel@truelite.it>  Tue, 11 May 2010 15:32:33 +0200

pyoctofuss (1.2.1) lenny; urgency=low

  * New upstream release

 -- Christopher R. Gabriel <cgabriel@truelite.it>  Fri, 09 Apr 2010 19:33:41 +0200

pyoctofuss (1.2.0) calibre5; urgency=low

  * Implemented 'ldoc' method in ListTree

 -- Enrico Zini <enrico@truelite.it>  Fri, 17 Jul 2009 13:53:04 +0100

pyoctofuss (1.1.2) calibre5; urgency=low

  * version bump

 -- Christopher R. Gabriel <cgabriel@truelite.it>  Thu, 16 Jul 2009 12:10:53 +0200

pyoctofuss (1.1.1) calibre5; urgency=low

  * Fixed dependencies

 -- Enrico Zini <enrico@truelite.it>  Fri, 10 Jul 2009 16:35:03 +0100

pyoctofuss (1.1) calibre5; urgency=low

  * Upload to calibre5 (5.0.3)

 -- Antonio J. Russo <antonio-externe.russo@edf.fr>  Thu,  9 Jul 2009 16:09:29 +0200

pyoctofuss (1.0) UNRELEASED; urgency=low

  * Initial release.

 -- Enrico Zini <enrico@truelite.it>  Fri, 26 Jun 2009 13:04:59 +0800
