# coding: utf-8
from __future__ import (absolute_import, print_function, division, unicode_literals)
import unittest
import octofuss

class StringTree(octofuss.Tree):
    "Simple tree wrapped around a dict, to use for tests"
    def __init__(self, name):
        super(StringTree, self).__init__(name)
        self.dict = {}

    def lhas(self, path):
        if not path: return True
        if len(path) != 1: raise KeyError("StringTree only contains leaf notes")
        return path[0] in self.dict

    def lget(self, path):
        if not path: return dict(self.dict)
        if len(path) != 1: raise KeyError("StringTree only contains leaf notes")
        return self.dict.get(path[0], None)

    def lset(self, path, value):
        if not path:
            raise KeyError("cannot set the value of path %s" % self._name)
        if len(path) != 1:
            raise KeyError("StringTree only contains leaf notes")
        elif not self.has(path[0]):
            raise KeyError("path %s does not exist" % path[0])
        else:
            self.dict[path[0]] = value

    def llist(self, path):
        if not path:
            return list(set([i.strip('/').split('/')[0] for i in self.dict.keys()]))
        elif len(path) != 1:
            raise KeyError("StringTree only contains leaf notes")
        else:
            return [i[len(path[0]) + 1:] for i in self.dict.keys() if i.startswith(path[0] + "/")]

    def ldelete(self, path):
        if len(path) != 1: raise KeyError("StringTree only contains leaf notes")
        self.dict.pop(path[0], None)

    def lcreate(self, path, value=None):
        if len(path) != 1: raise KeyError("StringTree only contains leaf notes")
        if self.has(path[0]):
            raise KeyError("path %s already exists" % path[0])
        self.dict[path[0]] = value


class TestTree(unittest.TestCase):
    def setUp(self):
        self.forest = octofuss.Forest()
        self.forest.register(StringTree("people"))
        self.forest.register(StringTree("buildings"))
        self.forest.create("/people/enrico", "12345")

    def testMissing(self):
        """
        Test proper behaviour with missing nodes
        """
        self.assertEquals(self.forest.has("/toys"), False)
        self.assertEquals(self.forest.has("/toys/pippo"), False)

        self.assertEquals(self.forest.get("/toys"), None)
        self.assertEquals(self.forest.get("/toys/pippo"), None)

        self.assertRaises(KeyError, self.forest.set, "/toys", 1)
        self.assertRaises(KeyError, self.forest.set, "/toys/pippo", 1)
        self.assertRaises(KeyError, self.forest.set, "/people", 1)

        self.assertRaises(KeyError, self.forest.list, "/toys")
        self.assertRaises(KeyError, self.forest.list, "/toys/pippo")

        self.assertRaises(KeyError, self.forest.delete, "/toys")
        self.assertRaises(KeyError, self.forest.delete, "/toys/pippo")
        self.assertRaises(KeyError, self.forest.delete, "/people")

        self.assertRaises(KeyError, self.forest.create, "/toys")
        self.assertRaises(KeyError, self.forest.create, "/toys/pippo")
        self.assertRaises(KeyError, self.forest.create, "/people", 1)

    def testNormal(self):
        """
        Test normal operations
        """

        self.assertEquals(self.forest.has("/people"), True)
        self.assertEquals(self.forest.has("/people/enrico"), True)
        self.assertEquals(self.forest.has("/people/pippo"), False)

        self.assertEquals(self.forest.get("/people/enrico"), "12345")
        self.assertEquals(self.forest.get("/people/pippo"), None)

        self.forest.set("/people/enrico", "54321")
        self.assertEquals(self.forest.get("/people/enrico"), "54321")
       
        self.assertEquals(set([x for x in self.forest.list("/")]), set(["people", "buildings"]))
        self.assertEquals([x for x in self.forest.list("/people")], ["enrico"])
        self.assertEquals([x for x in self.forest.list("/people/enrico")], [])

        self.forest.delete("/people/enrico")
        self.assertEquals(self.forest.has("/people/enrico"), False)
        self.assertEquals(self.forest.has("/people"), True)

        self.forest.create("/people/pippo", "abcde")
        self.assertEquals(self.forest.has("/people/pippo"), True)
        self.assertEquals(self.forest.get("/people/pippo"), "abcde")

class TestReadonlyDictTree(unittest.TestCase):
    def setUp(self):
        self.tree = octofuss.ReadonlyDictTree()
        sub = octofuss.ReadonlyDictTree("people")
        sub["name"] = "Enrico"
        self.tree.register(sub)
        self.tree["val"] = "antani"

    def testMissing(self):
        """
        Test proper behaviour with missing nodes
        """
        self.assertEquals(self.tree.has("/toys"), False)
        self.assertEquals(self.tree.has("/toys/pippo"), False)

        self.assertEquals(self.tree.get("/toys"), None)
        self.assertEquals(self.tree.get("/toys/pippo"), None)

        self.assertRaises(KeyError, self.tree.set, "/toys", 1)
        self.assertRaises(KeyError, self.tree.set, "/toys/pippo", 1)
        self.assertRaises(KeyError, self.tree.set, "/people", 1)
        self.assertRaises(KeyError, self.tree.set, "/val", 1)

        self.assertRaises(KeyError, self.tree.list, "/toys")
        self.assertRaises(KeyError, self.tree.list, "/toys/pippo")

        self.assertRaises(KeyError, self.tree.delete, "/toys")
        self.assertRaises(KeyError, self.tree.delete, "/toys/pippo")
        self.assertRaises(KeyError, self.tree.delete, "/people")
        self.assertRaises(KeyError, self.tree.delete, "/val")

        self.assertRaises(KeyError, self.tree.create, "/toys")
        self.assertRaises(KeyError, self.tree.create, "/toys/pippo")
        self.assertRaises(KeyError, self.tree.create, "/people", 1)
        self.assertRaises(KeyError, self.tree.create, "/val", 1)

    def testNormal(self):
        """
        Test normal operations
        """

        self.assertEquals(self.tree.has("/people"), True)
        self.assertEquals(self.tree.has("/people/name"), True)
        self.assertEquals(self.tree.has("/people/age"), False)

        self.assertEquals(self.tree.get("/people/name"), "Enrico")
        self.assertEquals(self.tree.get("/people/age"), None)
        self.assertEquals(self.tree.get("/val"), "antani")
        self.assertEquals(self.tree.get("/lav"), None)

        self.assertEquals(set([x for x in self.tree.list("/")]), set(["people", "val"]))
        self.assertEquals([x for x in self.tree.list("/people")], ["name"])
        self.assertEquals([x for x in self.tree.list("/people/name")], [])


class TestDictTree(unittest.TestCase):
    def test_set(self):
        tree = octofuss.DictTree("test")
        with self.assertRaises(KeyError): tree.set("foo", 1)
        tree.create("foo", 1)
        self.assertEquals(tree.get("foo"), 1)
        tree.set("foo", 2)
        self.assertEquals(tree.get("foo"), 2)
        tree.delete("foo")
        self.assertIsNone(tree.get("foo"))


class TestMockTree(unittest.TestCase):
    def test_empty(self):
        tree = octofuss.MockTree("test")

        self.assertEquals(tree.has(""), True)
        self.assertEquals(tree.has("foo"), False)
        self.assertEquals(tree.has("foo/bar"), False)

        self.assertEquals(tree.get(""), None)
        self.assertEquals(tree.get("foo"), None)
        self.assertEquals(tree.get("foo/bar"), None)

        self.assertEquals(tree.list(""), [])
        with self.assertRaises(KeyError): self.assertEquals(tree.list("foo"), [])
        with self.assertRaises(KeyError): self.assertEquals(tree.list("foo/bar"), [])

        tree.set("", 1)
        self.assertEquals(tree.get(""), 1)

        tree.create("foo", 2)
        self.assertEquals(tree.get(""), { "foo": 2 })
        self.assertEquals(tree.get("foo"), 2)

        tree.create("foo/bar", 3)
        self.assertEquals(tree.get(""), { "foo": { "bar": 3 } })
        self.assertEquals(tree.get("foo"), { "bar": 3 })
        self.assertEquals(tree.get("foo/bar"), 3)

        tree.create("foo/baz", { "gnu": 4 })
        self.assertEquals(tree.get("foo/baz"), { "gnu": 4 })
        self.assertEquals(tree.get("foo/baz/gnu"), 4)

        tree.delete("foo")
        self.assertEquals(tree.get(""), {})
        self.assertEquals(tree.get("foo"), None)
