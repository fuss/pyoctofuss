# coding: utf-8
from __future__ import (absolute_import, print_function, division, unicode_literals)
import unittest
import io
import octofuss.samba as osmb

class TestTree(unittest.TestCase):
    def setUp(self):
        self.buf = io.StringIO("""
        [global]
        workgroup=test

        [cdrom]
        path=/cdrom
        read only=yes
        guest ok=yes
        write list=@wizards

        [public]
        path=/public
        read only=no
        guest ok=no

        [news]
        path=/news
        read only=yes
        write list=@editors

        [cabal]
        path=/cabal
        read only=yes
        read list=@cabal
        write list=@cabal
        """)
        self.conf = osmb.SambaConf()
        self.conf.ReadConfig("smb.conf", dummyfd=self.buf)

    def getParam(self, dic, name):
        if name not in dic:
            self.fail("%s is not one of %s" % (name, ", ".join(dic.keys())))
        return dic[name].getValue()

    def testRead(self):
        """
        Test reading the configuration
        """
        shares = self.conf.Services()
        self.assertEquals(sorted(shares), ["CABAL", "CDROM", "GLOBAL", "NEWS", "PUBLIC"])
        s = self.conf.getService("CABAL")
        self.assertEquals(self.getParam(s, "PATH"), "/cabal")
        self.assertEquals(self.getParam(s, "READONLY"), "yes")
        self.assertEquals(self.getParam(s, "READLIST"), "@cabal")
        self.assertEquals(self.getParam(s, "WRITELIST"), "@cabal")

    def testWrite(self):
        """
        Test editing
        """
        self.conf.AddService("test")
        self.conf.SetServiceOption("test", "path", "/test")
        self.conf.SetServiceOption("test", "readonly", "false")
        buf = io.StringIO()
        self.conf.Write("smb1.conf", dummyfd=buf)
        conf = buf.getvalue()
        assert(conf.find("[TEST]") != -1)

