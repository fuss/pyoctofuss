import unittest
import octofuss.serialize as ser
import datetime
from pytz import utc

class PLUS1(datetime.tzinfo):
    """UTC timezone that the python developers wrote in the examples but did
    not bother to include in the standard library. Obviously, it is useless."""

    ZERO = datetime.timedelta(0)
    OFS = datetime.timedelta(hours=1)

    def utcoffset(self, dt):
        return self.OFS
    def tzname(self, dt):
        return "GMT+1"
    def dst(self, dt):
        return self.ZERO
plus1 = PLUS1()

class TestSerialize(unittest.TestCase):
    def testDatetime(self):
        """
        Test serialization of datetimes
        """
        a = datetime.datetime(2008, 7, 6, 5, 4, 3)
        sa = ser.dump_datetime(a)
        self.assertEquals(sa, "2008-07-06 05:04:03")
        a1 = ser.load_datetime(sa)
        self.assertEquals(a.replace(tzinfo=utc), a1)

    def testDatetimeWithTz(self):
        """
        Test serialization of datetimes with a timezone
        """
        a = datetime.datetime(2008, 7, 6, 5, 4, 3, tzinfo=plus1)
        sa = ser.dump_datetime(a)
        self.assertEquals(sa, "2008-07-06 04:04:03")
        a1 = ser.load_datetime(sa)
        # Here I need to put an astimezone because the unix timestamps,
        # according to the python standard libraries, change if I change the
        # timezone of a datetime while still having it refer to the same
        # instant.
        # That is, according to the python standard library, the timestamp of:
        # 2008-07-06 05:04:03+01:00 is different from the timestamp of:
        # 2008-07-06 04:04:03+00:00.
        self.assertEquals(a.astimezone(utc).strftime("%s"), a1.strftime("%s"))
