import unittest
import octofuss.helpers

class TestFindBroadcast(unittest.TestCase):
    def testNotCrashing(self):
        # just check that find_broadcast is not crashing and is
        # returning an iterable of strings, as the actual contents are
        # very environment dependent.
        bcasts = octofuss.helpers.find_broadcast()
        for addr in bcasts:
            self.assertIsInstance(addr, str)
