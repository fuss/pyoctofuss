import unittest
from octofuss.xmlrpc import ExceptionSerializer, APIKeyException, UnknownException
import octofuss.exceptions

class TestSerialize(unittest.TestCase):
    def setUp(self):
        self.ser = ExceptionSerializer()

    def test_builtin(self):
        d = self.ser.to_dict(ValueError("test"))
        self.assertEquals(d, {"name": "builtins.ValueError", "msg": "test"})

    def test_subclass(self):
        class TestError(KeyError):
            pass
        d = self.ser.to_dict(TestError("test"))
        self.assertEquals(d, {"name": "builtins.KeyError", "msg": "'test'"})

    def test_custom(self):
        d = self.ser.to_dict(APIKeyException("test"))
        self.assertEquals(d, {"name": "octofuss.xmlrpc.APIKeyException", "msg": "test"})

        d = self.ser.to_dict(octofuss.exceptions.HomeDirectoryExistsError("test"))
        self.assertEquals(d, {"name": "octofuss.exceptions.HomeDirectoryExistsError", "msg": "test"})

    def test_unknown(self):
        d = self.ser.to_dict(42)
        self.assertEquals(d, {"name": "octofuss.xmlrpc.UnknownException", "msg": "42"})

class TestDeserialize(unittest.TestCase):
    def setUp(self):
        self.ser = ExceptionSerializer()

    def test_builtin(self):
        e = self.ser.from_dict({"name": "builtins.ValueError", "msg": "test"})
        self.assertEquals(e.__class__, ValueError)
        self.assertEquals(e.args[0], "test")

    def test_custom(self):
        e = self.ser.from_dict({"name": "octofuss.xmlrpc.APIKeyException", "msg": "test"})
        self.assertEquals(e.__class__, APIKeyException)
        self.assertEquals(e.args[0], "test")

        e = self.ser.from_dict({"name": "octofuss.exceptions.HomeDirectoryExistsError", "msg": "test"})
        self.assertEquals(e.__class__, octofuss.exceptions.HomeDirectoryExistsError)
        self.assertEquals(e.args[0], "test")

    def test_unknown(self):
        e = self.ser.from_dict({"name": "octofuss.xmlrpc.UnknownException", "msg": "42"})
        self.assertEquals(e.__class__, UnknownException)
        self.assertEquals(e.args[0], "42")
